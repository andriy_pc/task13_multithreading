package functions;

import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.jupiter.api.Assertions.*;

class FiboSumTest {

    @Test
    void perform()
            throws NoSuchMethodException, InvocationTargetException,
            IllegalAccessException {
        FiboSum fs = new FiboSum();
        Method m = fs.getClass().getDeclaredMethod("perform", int.class);
        m.setAccessible(true);
        m.invoke(fs, 5);
    }
}