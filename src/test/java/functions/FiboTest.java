package functions;

import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static org.junit.jupiter.api.Assertions.*;

class FiboTest {

    @Test
    void perform() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Fibo f = new Fibo();
        Method pf = f.getClass()
                .getDeclaredMethod("perform", int.class);
        pf.setAccessible(true);
        pf.invoke(f, 5);
    }
}