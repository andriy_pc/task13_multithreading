package mvc;

import java.util.concurrent.ExecutionException;
import functions.*;

public class Model {
    private PingPong pong = new PingPong();
    private Fibo fibo = new Fibo();
    private FiboSum fiboSum = new FiboSum();
    private Sleeper sleeper = new Sleeper();
    private Synchronization synchronization
            = new Synchronization();
    private Communication communication =
            new Communication();
    private CommunicatePipe communicatePipe =
            new CommunicatePipe();
    private Locks locsk = new Locks();

    public void pingPong() {
        pong.perform();
    }

    public void fiboNumbs() {
        fibo.perform();
    }

    public void fiboSum() {
        try {
            fiboSum.perform();
        } catch (InterruptedException ie) {
            ie.printStackTrace();
            throw new RuntimeException(ie.getMessage());
        } catch (ExecutionException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }

    }

    public void sleep() {
        sleeper.perform();
    }

    public void synchronize() {
        try {
            synchronization.perform();
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }

    }

    public void communicateWithQueue() {
        communication.perform();
    }

    public void communicateWithPipe() {
        communicatePipe.perform();
    }

    public void useLocks() {
        locsk.perform();
    }
}
