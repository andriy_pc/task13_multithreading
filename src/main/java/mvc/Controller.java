package mvc;

import mvc.view.Display;
import mvc.view.Viewer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Controller {
    private static Logger log = LogManager.getLogger();
    private Model model;
    private Viewer viewer;
    private Map<Integer, Display> functions;
    private Scanner in;

    public Controller(Model m, Viewer v) {
        model = m;
        viewer = v;
        in = new Scanner(System.in);
        initFunctions();
    }

    private void initFunctions() {
        functions = new LinkedHashMap<>();
        functions.put(0, this::quit);
        functions.put(1, model::pingPong);
        functions.put(2, model::fiboNumbs);
        functions.put(3, model::fiboSum);
        functions.put(4, model::sleep);
        functions.put(5, model::synchronize);
        functions.put(6, model::communicateWithQueue);
        functions.put(7, model::useLocks);
        functions.put(8, model::communicateWithPipe);
    }

    public void run() {
        int choice = 0;
        viewer.display();
        try {
            choice = in.nextInt();
            functions.get(choice).display();
        } catch (IndexOutOfBoundsException b) {
            log.error("out of bounds in functions");
            throw new RuntimeException("out of bounds in functions");
        }

    }

    private void quit() {
        System.out.println("[Exit]");
        log.info("user quit");
        System.exit(0);
    }
}
