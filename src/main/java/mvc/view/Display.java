package mvc.view;

import java.util.concurrent.ExecutionException;

public interface Display {
    void display();
}
