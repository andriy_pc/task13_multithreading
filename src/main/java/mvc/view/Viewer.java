package mvc.view;

import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class Viewer {
    private static Logger log = LogManager.getLogger();
    private List<String> menu;

    public Viewer() {
        menu = new ArrayList<>();
        initMenu();
    }

    public void initMenu() {
        menu.add("0 quit");
        menu.add("1 ping-pong");
        menu.add("2 Fibonacci numbers");
        menu.add("3 sum of Fibonacci numbers");
        menu.add("4 sleeper task");
        menu.add("5 synchronize on one/multiply objects");
        menu.add("6 use blockingQueue to communicate between threads");
        menu.add("7 use locks to synchronize");
        menu.add("8 use pipe to communicate between threads");
    }

    public void display() {
        System.out.println("select option");
        for (String s : menu) {
            System.out.println(s);
        }
    }
}
