package functions;

import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Sleeper {
    private Scanner in = new Scanner(System.in);
    public void perform() {
        System.out.println("Insert quantity of tasks");
        int qty = in.nextInt();

        ScheduledExecutorService exec = Executors.newScheduledThreadPool(qty);
        for(int i = 0; i < qty; ++i) {
            exec.schedule(new Nap(), 0, TimeUnit.SECONDS);
        }
        exec.shutdown();
    }
}

class Nap implements Runnable {
    private Random rand = new Random();
    int n;
    public void run() {
        try {
            n = rand.nextInt(11);
            TimeUnit.SECONDS.sleep(n);
            System.out.println("nap time: " + n + ";");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
