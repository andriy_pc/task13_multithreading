package functions;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Pipe;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


public class CommunicatePipe {
    public void perform() {
        try {
            Pipe p = Pipe.open();
            Sender sender = new Sender(p);
            Receiver receiver = new Receiver(p);
            ExecutorService exec = Executors.newFixedThreadPool(2);
            exec.execute(sender);
            exec.execute(receiver);
            exec.shutdown();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


class Sender implements Runnable {
    private Pipe pipe;
    private Pipe.SinkChannel sender;
    ByteBuffer buf = ByteBuffer.allocate(16);
    public Sender(Pipe p) {
        pipe = p;
        sender = pipe.sink();
    }
    public void run() {
        try {
            int t = 0;
            for(int i = 0; i < 3; i++) {
                for(int j = 1; j <= 15; ++j) {
                    buf.put((byte)++t);
                }
                buf.flip();
                sender.write(buf);
                buf.clear();
                TimeUnit.MILLISECONDS.sleep(250);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException i) {
            i.printStackTrace();
        }

    }
}

class Receiver implements Runnable {
    private Pipe pipe;
    private Pipe.SourceChannel source;
    ByteBuffer buf = ByteBuffer.allocate(16);
    public Receiver(Pipe p) {
        pipe = p;
        source = pipe.source();
    }
    public void run() {
        try {
            for(int i = 0; i < 3; i++) {
                System.out.println("reading");
                source.read(buf);
                buf.flip();
                while(buf.hasRemaining()) {
                    System.out.println(buf.get());
                }
                buf.clear();
                TimeUnit.MILLISECONDS.sleep(0);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException i) {
            i.printStackTrace();
        }
    }
}