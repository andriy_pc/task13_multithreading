package functions;

import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Fibo {
    private Scanner in = new Scanner(System.in);

    public void perform() {
        System.out.println("Insert n");
        int n = in.nextInt();
        ExecutorService exec = Executors.newFixedThreadPool(n);
        for(int i = 0; i < n; ++i) {
            exec.execute(new Task(n));
        }
    }
}

class Task implements Runnable {
    private int n;
    public Task(int qty) {
        n = qty;
    }
    public void run() {
        int cur = 1;
        int prev = 0;
        int tmp;
        for (int i = 0; i < n; i++) {
            System.out.print(Thread.currentThread().getId()
                    + "(" + cur + "); ");
            tmp = cur;
            cur = tmp + prev;
            prev = tmp;
        }

    }
}
