package functions;

import java.util.InputMismatchException;
import java.util.concurrent.*;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Communication {
    private static Logger log = LogManager.getLogger();
    private static BlockingQueue<Integer> resource;
    public Communication() {
        resource = new ArrayBlockingQueue<>(5);
    }

    public void perform() {
        //Is responsible for sending Integer to another thread
        Runnable r1 = () -> {
            Scanner in = new Scanner(System.in);
            int transfer = 0;
            if(resource == null) {
                log.fatal("resource queue in Communication is null!");
                throw new NullPointerException("resource queue" +
                        " in Communication is null!");
            }
            try {
                for(int i = 0; i < 15; ++i) {
                    System.out.println("Insert integer to transfer");
                    System.out.println("Insert -1 to quit");
                    transfer = in.nextInt();
                    TimeUnit.MILLISECONDS.sleep(50);
                    System.out.println(Thread.currentThread().getName() + " puts: " + transfer);
                    resource.put(transfer);
                }
            } catch(InterruptedException e) {
                StackTraceElement[] stack = e.getStackTrace();
                for(StackTraceElement ste : stack) {
                    log.error(ste);
                }
            } catch (InputMismatchException me) {
                log.error("wrong input");
            }
        };
        //Is responsible for retrieving Integer to another thread
        Runnable r2 = () -> {
            if(resource == null) {
                log.fatal("resource queue in Communication is null!");
                throw new NullPointerException("resource queue" +
                        " in Communication is null!");
            }
            try {
                TimeUnit.MILLISECONDS.sleep(2500);
                for(int i = 1; i < 15; ++i) {
                    System.out.println(Thread.currentThread().getName()
                            + " gets: " + resource.take());
                }
            } catch(InterruptedException e) {
                StackTraceElement[] stack = e.getStackTrace();
                for(StackTraceElement ste : stack) {
                    log.error(ste);
                }
            }
        };

        ExecutorService exec = Executors.newFixedThreadPool(2);
        exec.execute(r1);
        exec.execute(r2);
        exec.shutdown();
    }
}
