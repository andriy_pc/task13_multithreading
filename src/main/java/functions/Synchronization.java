package functions;

import java.time.LocalTime;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

class Task6 {
    private static Logger log = LogManager.getLogger();
    private Object obj = new Object();
    public void startAll() {
        try {
            first();
            second();
            third();
        } catch (InterruptedException e) {
            StackTraceElement[] stack = e.getStackTrace();
            for(StackTraceElement ste : stack) {
                log.error(ste);
            }
        }

    }
    private void first() throws InterruptedException {
        synchronized(obj) {
            TimeUnit.MILLISECONDS.sleep(1000);
            System.out.println("first method, " +
                    Thread.currentThread().getName() +
                    " " + LocalTime.now());
        }
    }
    private void second() throws InterruptedException {
        synchronized(obj) {
            TimeUnit.MILLISECONDS.sleep(1000);
            System.out.println("second method, " +
                    Thread.currentThread().getName() +
                    " " + LocalTime.now());
        }
    }
    private void third() throws InterruptedException {
        synchronized(obj) {
            TimeUnit.MILLISECONDS.sleep(1000);
            System.out.println("third method, " +
                    Thread.currentThread().getName() +
                    " " + LocalTime.now());
        }
    }
}

public class Synchronization {
    public void perform() throws InterruptedException {
        Task6 t = new Task6();
        Multiple m = new Multiple();
        Runnable r1 = () -> {
            t.startAll();
        };
        Runnable r2 = () -> {
            m.startAll();
        };

        ExecutorService exec = Executors.newFixedThreadPool(5);
        for(int i = 0; i < 5; ++i) {
            exec.execute(r1);
        }
        TimeUnit.SECONDS.sleep(16);
        System.out.println("--------Multiple Objects" +
                " to synchronize on--------");
        for(int i = 0; i < 5; ++i) {
            exec.execute(r2);
        }
        exec.shutdown();
    }
}

class Multiple {
    private Object obj = new Object();
    private Object obj2 = new Object();
    private Object obj3 = new Object();
    public void startAll() {
        try {
            first();
            second();
            third();
        } catch (InterruptedException e) {
        }

    }
    private void first() throws InterruptedException {
        synchronized(obj) {
            TimeUnit.MILLISECONDS.sleep(1000);
            System.out.println("Mfirst method, " +
                    Thread.currentThread().getName() +
                    " " + LocalTime.now());
        }
    }
    private void second() throws InterruptedException {
        synchronized(obj2) {
            TimeUnit.MILLISECONDS.sleep(1000);
            System.out.println("Msecond method, " +
                    Thread.currentThread().getName() +
                    " " + LocalTime.now());
        }
    }
    private void third() throws InterruptedException {
        synchronized(obj3) {
            TimeUnit.MILLISECONDS.sleep(1000);
            System.out.println("Mthird method, " +
                    Thread.currentThread().getName() +
                    " " + LocalTime.now());
        }
    }
}