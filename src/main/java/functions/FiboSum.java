package functions;

import java.util.Scanner;
import java.util.concurrent.*;

public class FiboSum {
    private ExecutorService exec = Executors.newFixedThreadPool(2);

    private Scanner in = new Scanner(System.in);
    public void perform() throws ExecutionException, InterruptedException {
        System.out.println("Insert value of numbers");
        int val = in.nextInt();
        Future<Integer> r1 = exec.submit(new Sum(val));
        Future<Integer> r2 = exec.submit(new Sum(val));

        System.out.println("r1: " + r1.get());
        System.out.println("r2: " + r2.get());
    }
}

class Sum implements Callable<Integer> {
    private int n;
    public Sum(int qty) {
        n = qty;
    }
    public Integer call() {
       Integer result = 0;
        int cur = 1;
        int prev = 0;
        int tmp;
        for (int i = 0; i < n; i++) {
            result += cur;
            tmp = cur;
            cur = tmp + prev;
            prev = tmp;
        }
        return result;
    }
}


