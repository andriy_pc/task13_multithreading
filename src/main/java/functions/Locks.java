package functions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalTime;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.StampedLock;



public class Locks {

    public void perform() {
        try {
            OneObjLock o = new OneObjLock();
            MultipleObjLock m = new MultipleObjLock();

            Runnable r1 = () -> {
                o.startAll();
            };
            Runnable r2 = () -> {
                m.startAll();
            };
            ExecutorService exec = Executors.newFixedThreadPool(5);
            for(int i = 0; i < 5; ++i) {
                exec.execute(r1);
            }
            TimeUnit.SECONDS.sleep(16);
            System.out.println("--------Multiple Objects" +
                    " to Lock on--------");
            for(int i = 0; i < 5; ++i) {
                exec.execute(r2);
            }
            exec.shutdown();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}

class OneObjLock {
    private static Logger log = LogManager.getLogger();

    private Object obj = new Object();
    private ReentrantLock lock = new ReentrantLock();

    public void startAll() {
        lock.lock();
        try {
            first();
            second();
            third();
        } catch (InterruptedException e) {
            StackTraceElement[] stack = e.getStackTrace();
            for(StackTraceElement ste : stack) {
                log.error(ste);
            }
        } finally {
            lock.unlock();
        }

    }
    private void first() throws InterruptedException {
        synchronized(obj) {
            TimeUnit.MILLISECONDS.sleep(1000);
            System.out.println("first method, " +
                    Thread.currentThread().getName() +
                    " " + LocalTime.now());
        }
    }
    private void second() throws InterruptedException {
        synchronized(obj) {
            TimeUnit.MILLISECONDS.sleep(1000);
            System.out.println("second method, " +
                    Thread.currentThread().getName() +
                    " " + LocalTime.now());
        }
    }
    private void third() throws InterruptedException {
        synchronized(obj) {
            TimeUnit.MILLISECONDS.sleep(1000);
            System.out.println("third method, " +
                    Thread.currentThread().getName() +
                    " " + LocalTime.now());
        }
    }
}

class MultipleObjLock {
    private Object obj = new Object();
    private Object obj2 = new Object();
    private Object obj3 = new Object();
    private StampedLock sLock = new StampedLock();

    public void startAll() {
        long stamp = sLock.readLock();
        try {
            first();
            second();
            third();
        } catch (InterruptedException e) {
        } finally {
            sLock.unlock(stamp);
        }

    }
    private void first() throws InterruptedException {
        synchronized(obj) {
            TimeUnit.MILLISECONDS.sleep(1000);
            System.out.println("Mfirst method, " +
                    Thread.currentThread().getName() +
                    " " + LocalTime.now());
        }
    }
    private void second() throws InterruptedException {
        synchronized(obj2) {
            TimeUnit.MILLISECONDS.sleep(1000);
            System.out.println("Msecond method, " +
                    Thread.currentThread().getName() +
                    " " + LocalTime.now());
        }
    }
    private void third() throws InterruptedException {
        synchronized(obj3) {
            TimeUnit.MILLISECONDS.sleep(1000);
            System.out.println("Mthird method, " +
                    Thread.currentThread().getName() +
                    " " + LocalTime.now());
        }
    }
}
