package functions;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class PingPong {

    public void perform() {
        ExecutorService exec = Executors.newCachedThreadPool();
        try {
            Car car = new Car();
            exec.execute(new WaxOff(car));
            exec.execute(new WaxOn(car));
            //to make some output
            TimeUnit.SECONDS.sleep(3);
            exec.shutdownNow();
        } catch(InterruptedException e) {
            e.printStackTrace();
        }
    }
}

class Car {
    private boolean waxOn = false;
    public synchronized void waxed() {
        waxOn = true;
        notifyAll();
    }

    public synchronized void buffed() {
        waxOn = false;
        notifyAll();
    }

    public synchronized void waitForWaxing()
            throws InterruptedException {
        while(waxOn == false) {
            wait();
        }
    }

    public synchronized void waitForBuffing()
        throws InterruptedException {
        while(waxOn == true) {
            wait();
        }
    }
}

class WaxOn implements Runnable {
    private Car car;
    public WaxOn(Car c) {
        car = c;
    }

    public void run() {
        try {
            while(!Thread.interrupted()) {
                System.out.println("wax on!");
                TimeUnit.MILLISECONDS.sleep(200);
                car.waxed();
                car.waitForBuffing();
            }
        } catch(InterruptedException e) {
            System.out.println("Exiting via interrupt");
        }
        System.out.println("Ending wax on task");
    }
}

class WaxOff implements Runnable {
    private Car car;
    public WaxOff(Car c) {
        car = c;
    }

    public void run() {
        try {
            while(!Thread.interrupted()) {
                car.waitForWaxing();
                System.out.println("wax off!");
                TimeUnit.MILLISECONDS.sleep(200);
                car.buffed();
            }
        } catch (InterruptedException e) {
            System.out.println("Exiting via interrupt");
        }
        System.out.println("Ending wax off task");
    }
}
