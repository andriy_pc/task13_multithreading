package app;

import mvc.*;
import mvc.view.*;

public class Application {
    public static void main(String[] args) {
        Controller c =
                new Controller(new Model(), new Viewer());
        while(true) {
            c.run();
        }
    }
}
